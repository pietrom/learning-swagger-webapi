﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace LearningSwagger.MeltingPot
{
    public class PeopleInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<PeopleRepository>().ImplementedBy<InMemoryPeopleRepository>());
        }
    }
}
