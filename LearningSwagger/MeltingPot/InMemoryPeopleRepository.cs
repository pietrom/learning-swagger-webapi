﻿using System.Collections.Generic;
using System.Linq;

namespace LearningSwagger.MeltingPot
{
    public class InMemoryPeopleRepository : PeopleRepository
    {
        private readonly IList<Person> people;

        public InMemoryPeopleRepository()
        {
            this.people = new List<Person>
            {
                new Person(1, "Pietro Martinelli"),
                new Person(2, "Pietro Martinelli"),
                new Person(3, "Pietro Martinelli"),
                new Person(4, "Pietro Martinelli")
            };
        }

        public IEnumerable<Person> FindAll()
        {
            return people;
        }

        public Person FindById(int id)
        {
            return people.SingleOrDefault(p => p.Id == id);
        }
    }
}
