﻿using System.Collections.Generic;

namespace LearningSwagger.MeltingPot
{
    public interface PeopleRepository
    {
        IEnumerable<Person> FindAll();

        Person FindById(int id);
    }
}