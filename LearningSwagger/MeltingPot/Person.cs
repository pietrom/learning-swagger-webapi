﻿namespace LearningSwagger.MeltingPot
{
    public class Person
    {
        public int Id { get; }
        public string FullName { get; }

        public Person(int id, string fullName)
        {
            Id = id;
            FullName = fullName;
        }
    }
}