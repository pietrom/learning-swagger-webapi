﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LearningSwagger.MeltingPot
{
    [RoutePrefix("api/v1/people")]
    public class PeopleController: ApiController
    {
        private readonly PeopleRepository repo;

        public PeopleController(PeopleRepository repo)
        {
            this.repo = repo;
        }

        [Route("")]
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            return Request.CreateResponse(HttpStatusCode.OK, repo.FindAll());
        }

        [Route("{id}")]
        [HttpGet]
        public HttpResponseMessage GetById(int id)
        {
            Person p = repo.FindById(id);
            return p == null ? Request.CreateResponse(HttpStatusCode.NotFound) :
                Request.CreateResponse(HttpStatusCode.OK, p);
        }
    }
}
