﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LearningSwagger.MeltingPot
{
    [RoutePrefix("api/echo")]
    public class EchoController : ApiController
    {
        [Route("")]
        [HttpGet]
        public HttpResponseMessage Echo(string msg)
        {
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }
    }
}
