﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LearningSwagger.MeltingPot
{
    [RoutePrefix("api/teapot")]
    public class TeapotController : ApiController
    {
        [Route("")]
        [HttpGet]
        public HttpResponseMessage IAmATeapot()
        {
            return Request.CreateResponse((HttpStatusCode) 418);
        }
    }
}
