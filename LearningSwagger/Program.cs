﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Dependencies;
using Castle.MicroKernel;
using Castle.MicroKernel.Lifestyle;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Hosting;
using Newtonsoft.Json.Serialization;
using Owin;
using Swashbuckle.Application;

namespace LearningSwagger {
    internal class Program {
        public static void Main(string[] args) {
            WindsorContainer container = BuildContainer();
            var server = WebApp.Start(new StartOptions("http://localhost:2020"), new Startup(container).Configuration);
            Console.WriteLine("Server up - Press any key to end");
            Console.ReadKey();
        }

        private static WindsorContainer BuildContainer()
        {
            WindsorContainer container = new WindsorContainer();
            container.Install(FromAssembly.InThisApplication());
            return container;
        }
    }

    public class ControllersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyContaining<Startup>().BasedOn<ApiController>().LifestyleScoped());
        }
    }

    internal class Startup
    {
        private readonly WindsorContainer container;

        public Startup(WindsorContainer container)
        {
            this.container = container;
        }

        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
            config.MapHttpAttributeRoutes();

            config.DependencyResolver = BuildDependencyResolver(container);

            config.EnsureInitialized();

            app.UseCors(CorsOptions.AllowAll);
            config.EnableSwagger(c =>
            {
                c.SingleApiVersion("v1", "Learning Swagger");
                //c.ResolveConflictingActions(x => x.First());
            }).EnableSwaggerUi();
            app.UseWebApi(config);
        }

        private static CastleWindsorDependencyResolver BuildDependencyResolver(WindsorContainer container)
        {
            return new CastleWindsorDependencyResolver(container.Kernel);
        }
    }

    internal class CastleWindsorDependencyResolver : System.Web.Http.Dependencies.IDependencyResolver
    {
        private readonly IKernel kernel;

        public CastleWindsorDependencyResolver(IKernel kernel)
        {
            this.kernel = kernel;
        }

        public IDependencyScope BeginScope()
        {
            return new CastleWindsorDependencyScope(kernel);
        }

        public object GetService(Type type)
        {
            return kernel.HasComponent(type) ? kernel.Resolve(type) : null;
        }

        public IEnumerable<object> GetServices(Type type)
        {
            return kernel.ResolveAll(type).Cast<object>();
        }

        public void Dispose()
        {
        }
    }

    public class CastleWindsorDependencyScope : IDependencyScope
    {
        private readonly IKernel kernel;

        private readonly IDisposable disposable;

        public CastleWindsorDependencyScope(IKernel kernel)
        {
            this.kernel = kernel;
            disposable = kernel.BeginScope();
        }

        public object GetService(Type type)
        {
            return kernel.HasComponent(type) ? kernel.Resolve(type) : null;
        }

        public IEnumerable<object> GetServices(Type type)
        {
            return kernel.ResolveAll(type).Cast<object>();
        }

        public void Dispose()
        {
            disposable.Dispose();
        }
    }
}